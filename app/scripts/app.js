(function(){
	'use strict';
	angular.module('youse_frontend',[
    'youse.config',
    'ui.router',
    'formly', 
    'formlyBootstrap',
    'ng-token-auth',
    'ngAnimate',
    'ngResource',
    'ngMessages'
	])
  .config(['formlyConfigProvider',function (formlyConfigProvider) {
    formlyConfigProvider.setWrapper({
      name: 'validation',
      types: ['input'],
      templateUrl: 'error-messages.html'
    });
  }])
	.run(['$rootScope','formlyConfig','formlyValidationMessages','$state',function($rootScope,formlyConfig,formlyValidationMessages,$state){
    formlyConfig.extras.errorExistsAndShouldBeVisibleExpression = 'fc.$touched || form.$submitted';
    formlyValidationMessages.addStringMessage('required', 'Este campo es obligatorio');

    $rootScope.$state = $state;

		$rootScope.app = {
      name: 'Youse Test',
      description: 'Frontend de pruba SOAT youse',
      year: ((new Date()).getFullYear()),
      layout: {
          rtl: false
      },
      sidebar: {
          over: false,
          showheader: true,
          showtoolbar: true,
          offcanvas: false
      },
      header: {
          menulink: 'menu-link-slide'
      },
      footerHidden: false,
      viewAnimation: 'ng-fadeInLeftShort',
      currentTheme: 0
    };


      /** Funciones de session ng-token auth **/
    $rootScope.$on('auth:login-success', function(ev, user) {
      $rootScope.$user = user;
    });
    $rootScope.$on('auth:validation-success', function(ev, user) {
      $rootScope.$user = user;
    });
    $rootScope.$on('auth:invalid', function(ev, reason) {
      console.info(ev);
      $state.transitionTo('user.login');
    });
    $rootScope.$on('auth:validation-error', function(ev, reason) {
      $state.transitionTo('user.login');
    });
    $rootScope.$on('auth:login-error', function(ev, reason) {
      $rootScope.alerts       = [];
      $rootScope.alerts.push({
        type: 'danger',
        msg: "Usuario y/o contraseña incorrecta."
      });
    });
    $rootScope.$on('auth:logout-error', function(ev, reason) {
      $state.transitionTo('user.login');
    });
    $rootScope.$on('auth:session-expired', function(ev) {
      $state.transitionTo('user.login');
    });

	}])
})();