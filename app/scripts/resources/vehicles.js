(function(){
  'use strict';
  angular.module('youse_frontend')
  .factory('Vehicles', ['API_CONF','$resource',function (API_CONF,$resource) {
    	var url = API_CONF.apiBaseUrl('vehicles/:id.json');
    	console.log(url);
      return $resource(url,{ id: '@id' },{
      	'update':{method: 'PUT'},
      	'show_with_soat':{method: 'GET',url: API_CONF.apiBaseUrl("vehicles/:id/with_soat.json")}
      });
	}])
})();