(function(){
  'use strict';
  angular.module('youse_frontend')
  .factory('Owners', ['API_CONF','$resource',function (API_CONF,$resource) {
    	var url = API_CONF.apiBaseUrl('owners/:id.json');
    	console.log(url);
      return $resource(url,{ id: '@id' },{
      	'update':{method: 'PUT'}
      });
	}])
})();