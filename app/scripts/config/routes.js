(function(){
  'use strict';
  angular.module('youse_frontend')
  .config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
  	$urlRouterProvider.otherwise("/consultasoat");
  	$stateProvider
    .state('user', {
        title: 'User',
        abstract: true,
        template: '<div class="container"><div ui-view class="ng-fadeInLeftShort"></div></div>',
        controller: 'devise as devise'
    })
    .state('user.login', {
        url: '/login',
        title: 'Login',
        templateUrl: 'views/devise/login.html',
    })
    .state('user.signup', {
        url: '/signup',
        title: 'Signup',
        templateUrl: 'views/devise/signup.html'
    })
  	.state('yousefrontend',{
  		abstract: true,
      controller: 'templateCommon as template',
  		templateUrl: "views/common/content.html",
  	})
  	.state('yousefrontend.consultasoat',{
  		url:'/consultasoat',
  		title: 'Consulta Soat',
  		controller: 'consultaSoat as cs',
  		templateUrl: 'views/consultas/soat.html',
  	})
    .state('yousefrontend.authenticated',{
      abstract: true,
      url:'/user',
      template: '<div class="container"><div ui-view class="ng-fadeInLeftShort"></div></div>',
      resolve: {
        auth: ['$auth','$state',function($auth,$state) {
          return $auth.validateUser().catch(function(err){
            console.info('not authenticated',err);
            $state.go('user.login');
          });
        }]
      },
    })
    .state('yousefrontend.authenticated.welcome',{
      url:'/panel',
      title: 'Youse Soat',
      resolve:{
        myOwners:['Owners',function(Owners){
          return Owners.query();
        }],
        myVehicles:['Vehicles',function(Vehicles){
          return Vehicles.query();
        }]
      },
      controller: 'welcomeController as welcome',
      templateUrl: 'views/user/welcome.html'
    })
    .state('yousefrontend.authenticated.owner',{
      abstract: true,
      url:'/owner',
    })
    .state('yousefrontend.authenticated.owner.new',{
      url:'/new',
      title: 'Nuevo Propietario',
      controller: 'owner as owner',
      templateUrl: 'views/user/owners/new.html'
    })
    .state('yousefrontend.authenticated.owner.edit',{
      url:'/edit/{id:int}',
      title: 'Editar Propietario',
      controller: 'owner as owner',
      templateUrl: 'views/user/owners/edit.html'
    })
    .state('yousefrontend.authenticated.vehicle',{
      abstract: true,
      url:'/vehicles',
      resolve:{
        myOwners:['Owners',function(Owners){
          return Owners.query();
        }],
        classVehicles:['API_CONF','$http',function(API_CONF,$http){
          var url = API_CONF.apiBaseUrl('typesvehicles/distinct.json');
          return $http.get(url).then(function successCallback(response) {
            return response;
          }, function errorCallback(response) {
            return response;
          });
        }]
      }
    })
    .state('yousefrontend.authenticated.vehicle.new',{
      url:'/new',
      title: 'Nuevo Vehiculo',
      controller: 'vehicles as vehicle',
      templateUrl: 'views/user/vehicles/new.html',
    })
    .state('yousefrontend.authenticated.vehicle.edit',{
      url:'/edit/{id:int}',
      title: 'Editar Vehiculo',
      controller: 'vehicles as vehicle',
      templateUrl: 'views/user/vehicles/edit.html',
    })


  }]);
})();