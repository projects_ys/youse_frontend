(function(){
  'use strict';

  //Rutas
  //PDFJS.workerSrc = 'scripts/vendor/pdfjs-dist/build/pdf.worker.js';

  // Informacion de entorno
  var runEnv = {
    isDevelopment: window.location.port === '9001' || window.location.port === '9000',
    isDevelopmentCloud: window.location.host === '',
    dummyHost: '',
    clientHostResourceUrl : ''
  };
  // Configuracion API Backend
  var api = {};
  api.hostname = function(){
    
    if(runEnv.isDevelopment === true){
      return 'https://youse-backend-yasuarez.c9users.io';
    }
    
    return 'https://youse-backend.herokuapp.com';
  }();
  api.apiName = 'youse_backend';
  api.apiVersion = 'v1';
  api.apiBaseUrl = function(path){
    return api.hostname + '/api/' + api.apiVersion + '/' + path;
  };


  angular.module('youse.config', [])
    .constant('RUNENV_CONF', runEnv)
    .constant('API_CONF', api)
}());

