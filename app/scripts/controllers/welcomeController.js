(function(){
  'use strict';
  angular
  .module('youse_frontend')
  .controller('welcomeController',welcomeController);

  welcomeController.$inject = ['myOwners','myVehicles','Owners','Vehicles','$state'];

	function welcomeController(myOwners,myVehicles,Owners,Vehicles,$state){	
		var vm = this;
		vm.owners = myOwners;
    vm.vehicles = myVehicles;
    /**
     * Funcion de eliminación de propietarios
     * @return {[type]} [description]
     */
    vm.deleteOwner = function(idToDel){
      var confirmDelete = confirm("Esta seguro que quiere eliminar este registro?");
      if(confirmDelete){
        Owners.remove({id:idToDel},function(response){
          $state.reload();
        },function (error) {
          console.error(error);
        });
      }
    }

    vm.deleteVehicle = function(idToDel){
      var confirmDelete = confirm("Esta seguro que quiere eliminar este registro?");
      if(confirmDelete){
        Vehicles.remove({id:idToDel},function(response){
          $state.reload();
        },function (error) {
          console.error(error);
        });
      }
    }
	}

})();