(function(){
  'use strict';
  angular
  .module('youse_frontend')
  .controller('owner',owner);

  owner.$inject = ['Owners','$state','$stateParams'];

	function owner(Owners,$state,$stateParams){	
		var vm = this;
    var types_document =  [
                            {'value':'c.c.','text':'Cedula de ciudadania'},
                            {'value':'t.i.','text':'Tarjeta de identidad'},
                            {'value':'c.e.','text':'Cedula de extranjeria'}
                          ];
    if($stateParams.id !== undefined){
      vm.info = Owners.get({id:$stateParams.id});
    }
    vm.fieldsFormOwner = [
      {
        key: 'id',
        type: 'input',
        templateOptions: {
          type: 'hidden',
          disabled:'disabled'
        }
      },
      {
        key: 'type_document',
        type: 'select',
        templateOptions:{
            label: 'Tipo de documento',
            options: types_document,
            valueProp: 'value',
            labelProp: 'text',
            required: true,
            focus:true,
            placeholder: 'Seleccione su tipo de documento'
        }
      },
      {
        key: 'document_number',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Numero de documento',
          placeholder: 'Ingrese su numero de documento',
          validate:true
        }
      },
      {
        key: 'names',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Nombre',
          placeholder: 'Ingrese sus nombres',
          validate:true
        }
      },
      {
        key: 'lastnames',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Apellidos',
          placeholder: 'Ingrese sus apellidos',
          validate:true
        }
      },
      {
        key: 'email',
        type: 'input',
        templateOptions: {
          type: 'email',
          required: true,
          label: 'Email',
          placeholder: 'Ingrese su email.',
          validate:true
        }
      },
      {
        key: 'mobilphone',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Celular',
          placeholder: 'Ingrese su celular.',
          validate:true
        }
      },
      
    ];
    vm.submitForm = function(){
      if(vm.info.id !== undefined){ //Edit
        vm.info.$update();
        $state.reload();
      }else{ //Create
        Owners.save(vm.info);
        $state.go('yousefrontend.authenticated.welcome');
      }
    }
		//vm.destination 
	}

})();