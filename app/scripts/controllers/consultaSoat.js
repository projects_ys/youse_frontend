(function(){
  'use strict';
  angular
  .module('youse_frontend')
  .controller('consultaSoat',consultaSoat);

  consultaSoat.$inject = ['Vehicles'];

	function consultaSoat(Vehicles){	
		var vm = this;
		vm.showResult = false;
		vm.queryPlate = {};
		vm.plateFields = [
			{
				key: 'plate',
	      type: 'input',
	      templateOptions: {
	        type: 'text',
	        label: 'Placa del vehiculo a consultar',
	        placeholder: 'Ingrese placa'
	      }
			},
		];

		vm.onSubmit = function(){
			var vehicle = Vehicles.show_with_soat({id: vm.queryPlate.plate},function(response){
				vm.query = response;
				if(vm.query.insures.length > 0){
					vm.soat = vm.query.insures.pop();
				}else{
					vm.soat = "No reporta seguro.";
				}
				
			});
			vm.showResult = true;
		}
	}

})();