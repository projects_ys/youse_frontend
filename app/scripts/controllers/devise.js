(function(){
  'use strict';
  angular.module('youse_frontend')
  .controller('devise',devise);

  devise.$inject = ['$rootScope','$scope', '$auth', '$state','$timeout','formlyVersion'];

  function devise($rootScope,$scope, $auth, $state,$timeout,formlyVersion){
    var vm = this;

    vm.view_message = false;
    vm.account      = {};
    vm.credentials  = {};

    vm.model = {};
    vm.options = {};
    
    vm.fieldsFormSignIn = [
      {
        key: 'email',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Email',
          placeholder: 'Ingrese su email',
          validate:true
        }
      },
      {
        key: 'password',
        type: 'input',
        templateOptions: {
          type: 'password',
          required: true,
          label: 'Contraseña',
          placeholder: 'Ingrese su contraseña',
          validate:true
        }
      },
    ];

    vm.fieldsFormSignUp = [
       {
        key: 'email',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Email',
          placeholder: 'Ingrese su email',
          validate:true
        }
      },
      {
        key: 'password',
        type: 'input',
        templateOptions: {
          type: 'password',
          required: true,
          label: 'Contraseña',
          placeholder: 'Ingrese su contraseña',
          validate:true
        }
      },
      {
        key: 'password_confirmation',
        type: 'input',
        templateOptions: {
          type: 'password',
          required: true,
          label: 'Confirmar contraseña',
          placeholder: 'Ingrese su contraseña',
          validate:true
        }
      },
    ];

    vm.register = function(){
      if (vm.formSignUp.$valid) {
        if(vm.account.password !== vm.account.password_confirmation){
          vm.alerts.push({
            type: 'danger',
            msg: 'Los datos de contraseña no coinciden.'
          });
          vm.account.password = '';
          vm.account.password_confirmation = '';
        }else{
          $auth.submitRegistration(vm.account)
          .then(function(resp) {
            $state.go('user.login');
          })
          .catch(function(resp) {
            console.info(resp);
          });
        }
      }
    }
    vm.login = function() {
      if (vm.formSignIn.$valid) {
        $auth.submitLogin(vm.credentials)
        .then(function(resp) {
          $state.go('yousefrontend.authenticated.welcome');
        })
        .catch(function(resp) {
          vm.view_message = true;
          /*
          vm.alerts.push({
            type: 'danger',
            msg: resp.errors[0]
          });
          */
        });

        $timeout(function () {
          vm.view_message = false;
        }, 2000);
      }    
    };
    vm.requestPassword = function(){
      $auth.requestPasswordReset({
        email:vm.credentials.email
      }).then(function(resp) {
        vm.typemsg = "success";
        if(resp.success == 'false'){
          vm.title_response = "No se ha podido enviar el mensaje";
        }else{
          vm.title_response = "Mensaje enviado";
        }      
        vm.view_message = true;
        vm.msg_response = resp.data.message;
      }).catch(function(resp) {
        vm.typemsg = "danger";
        vm.title_response = "No se ha podido enviar el mensaje";
        vm.view_message = true;
        vm.msg_response = resp.data.errors[0];
      });
      
      $timeout(function () {
        vm.view_message = false;
      }, 2000);
    }
    vm.resetPassword = function() {
      $auth.updatePassword({
        'password'              : vm.credentials.password,
        'password_confirmation' : vm.credentials.password_confirmation,
        'reset_password_token'  : $state.params.token
      })
      .then(function(resp) {
        vm.typemsg  = "success";
        vm.title_response = "Contraseña restablecida con exito";
        vm.msg_response = resp.data.data.message;
        vm.view_message = true;
        $timeout(function () {
          vm.view_message = false;
          vm.credentials = {};
          $state.go('devise.login');
        }, 2000);
      })
      .catch(function(resp) {
        vm.typemsg = "danger";
        vm.title_response = "No se ha podido restablecer la contraseña";
        if(resp.data.errors[0] != undefined){
          vm.msg_response = resp.data.errors[0];
        }else{
          vm.msg_response = resp.errors.full_messages;
        }
        vm.view_message = true;
      });

      $timeout(function () {
        vm.view_message = false;
      }, 2000);
    };
  }
})();

