(function(){
  'use strict';
  angular
  .module('youse_frontend')
  .controller('vehicles',vehicles);

  vehicles.$inject = ['myOwners','Vehicles','$state','$stateParams','classVehicles'];

	function vehicles(myOwners,Vehicles,$state,$stateParams,classVehicles){	
		var vm = this;
    classVehicles.data
//{"id":3,"plate":"ABC123","classvehicle":"Motos","passengers":2,"cylinder":180,"tons":0,"owner_id":3,"created_at":"2017-10-05T03:58:53.459Z","updated_at":"2017-10-05T03:58:53.459Z","type_vehicle_id":null,"model":4}

    if($stateParams.id !== undefined){
      vm.info = Vehicles.get({id:$stateParams.id});
    }
    vm.fieldsFormVehicles = [
      {
        key: 'id',
        type: 'input',
        templateOptions: {
          type: 'hidden',
          disabled:'disabled'
        }
      },
      {
        key: 'plate',
        type: 'input',
        templateOptions: {
          type: 'text',
          required: true,
          label: 'Placa',
          placeholder: 'Ingrese la placa',
          validate:true
        }
      },
      {
        key: 'classvehicle',
        type: 'select',
        templateOptions:{
            label: 'Clase de vehiculo',
            options: classVehicles.data,
            valueProp: 'classvehicle',
            labelProp: 'classvehicle',
            required: true,
            focus:true,
            placeholder: 'Seleccione su clase de vehiculo'
        }
      },
      {
        key: 'cylinder',
        type: 'input',
        templateOptions: {
          type: 'number',
          required: true,
          label: 'Cilindraje(c.c.)',
          placeholder: 'Ingrese cilindraje',
          validate:true
        }
      },
      {
        key: 'model',
        type: 'input',
        templateOptions: {
          type: 'number',
          required: true,
          label: 'Modelo(Año)',
          placeholder: 'Ingrese año de modelo',
          validate:true
        }
      },
      {
        key: 'passengers',
        type: 'input',
        templateOptions: {
          type: 'number',
          required: true,
          label: 'Pasajeros',
          placeholder: 'Cantidad de pasajeros',
          validate:true
        }
      },
      {
        key: 'tons',
        type: 'input',
        templateOptions: {
          type: 'number',
          label: 'Toneladas(Solo para carga)',
          placeholder: 'Toneladas(Solo para carga)',
          validate:true
        }
      },
      {
        key: 'owner_id',
        type: 'select',
        templateOptions:{
            label: 'Propietario',
            options: myOwners,
            valueProp: 'id',
            labelProp: 'names',
            required: true,
            focus:true,
            placeholder: 'Seleccione su tipo de documento'
        }
      },      
    ];
    vm.submitForm = function(){
      if(vm.info.id !== undefined){ //Edit
        vm.info.$update();
        $state.reload();
      }else{ //Create
        Vehicles.save(vm.info);
        $state.go('yousefrontend.authenticated.welcome');
      }
    }
		//vm.destination 
	}

})();