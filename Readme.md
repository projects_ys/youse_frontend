El flujo del sistema se interpreto y se intento desarrollar de la siguiente forma:

Porcentaje de terminado (70%)

## Flujo
Pantalla inicial: Permite validar una placa y si tiene seguro (No se alcanzo a realizar compra de seguros, en caso de haber vehiculos se debe ingresar al login.

Login: Permite registrarse y luego autenticarse contra la API.

Pantalla panel: Permite ingresar, editar y eliminar propietarios y vehiculos, la idea es que el usuario cree los datos del propietario y los del vehiculo, el sistema detecta automaticamente con los datos que tipo de SOAT necesita (Solo funciona con motos, pues no me dio el tiempo para cargar todos los datos y hacer cargas de peso y pasajeros). De esta forma un 3 panel permitiria escoger usuario y vehiculo (previamente creados) y luego darle comprar(Este proceso no se alcanzo a realizar).

Formularios: Formularios para ingreso/edicion de Propietarios y Vehiculos.

## Servidor local de desarrollo
El codigo contenido permite ejecutar un servidor local de desarrollo (node.js) que facilita el desarrollo
y despliegue del código de la aplicación. A fin de utilizar el servidor local de desarrollo, se debe 
instalar las dependencias declaradas en el archivo `package.json` ejecutando `npm install` desde la
la raíz de la aplicación.


Ejecutar los siguiente comandos, para cargar dependencias:

```sh
$ npm install
$ bower install


```


Herramientas relevantes programadas en Gulp: 

```sh
#Obligaciones para que funcionen los inject
npm install -g stream-handbook
npm install -g browser-sync

$ gulp inject
#Lee los contenidos *.js de la carpeta scripts y los incluye automaticamente en el index.html

$ gulp wiredep
#Lee los contenidos bower.json de cada paquete de instalacion bower y los incluye automaticamente en el index.html

```

Correr servidor: 

```sh
$ gulp

```

```sh
## Estructura de la aplicación


  .gitignore
  .temp #Carpeta utilizada por gulp para compilar less
  gulpfile.js
  bower.json
  package.json
  dist/ 
  node_modules/
  app
  ├── less/
  ├── scripts
  │   ├── config/
  │   ├── controllers/
  │   ├── resources/
  │   └── app.js
  ├── vendor #Reconfigurado bower, para incluir todo aqui
  ├── styles
  │   └── *.css
  ├── views
  │   └── module
  │       └── *.html
  └── index.html
```