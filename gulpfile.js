var gulp = require('gulp'),
    gulpsync = require('gulp-sync')(gulp),
	connect = require('gulp-connect'),
    clean = require('gulp-clean'),
	historyApiFallBack = require('connect-history-api-fallback'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    browserSync = require('browser-sync').create(),
	path = require('path'),
    inject = require('gulp-inject'),
    minifyCSS = require('gulp-minify-css'),
    concatCss = require('gulp-concat-css'),
    notify = require('gulp-notify'),
    concatJs = require('gulp-concat'),
    rename = require('gulp-rename'),
    wiredep = require('wiredep').stream;

var src = {
    css: './app/styles/**/*.css',
    js:  './app/scripts/**/*.js',
    html: './app/**/*.html',
    less: './app/less/**/*.js'
};

/*Servidor */
gulp.task('live',function(){
	connect.server({
		root: './app',
		port: 9000,
		livereload: true,
		middleware: function(connect, opt){
			return [historyApiFallBack({})];
		}
	});
    
});

gulp.task('browsersync', function() {
    console.log('Starting BrowserSync..');

    browserSync.init({
        server: {
            baseDir: './app'
        },
        port:9000
    });

});

//Inyeccion automatica (Revisar el default -> Sola se ejecuta bien)
gulp.task('inject',function(){
    var target = gulp.src('./app/index.html');
      // It's not necessary to read the files (will speed up things), we're only after their paths: 
      var sources = gulp.src(['./app/scripts/**/*.js', './app/styles/**/*.css'], {read: false});
     
     return target.pipe(inject(sources, {ignorePath: 'app'}))
        .pipe(gulp.dest('./app'));
});

gulp.task('less',function(){
    gulp.src('app/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./.temp/css'))
});
//Inyeccion de bower
gulp.task('wiredep',function(){
    gulp.src('./app/index.html')
    .pipe(wiredep({
        directory: './app/vendor',
        onError: function(err){
            console.log(err.code);
        }
    }))
    .pipe(gulp.dest('./app'));
});
//Unir y minificar CSS
gulp.task('css', function () 
{
  gulp.src('./.temp/css/**/*.css')
    .pipe(concatCss("app.css"))
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest("./app/styles/"))
});

gulp.task('watch',['browsersync'], function () {
    gulp.watch([src.html,src.css]).on('change', browserSync.reload);
    gulp.watch(['./app/scripts/**/*.js'],['inject']);
    gulp.watch(['./app/less/**/*.less'],['less']);
    gulp.watch(['./app/styles/**/*.css'],['inject']);
    gulp.watch(['./.temp/css/**/*.less'],['css']);
    gulp.watch(['./bower.json'],['wiredep']);
});



gulp.task('default',['less','css','wiredep','live','watch']);

//Compile

gulp.task('clean', function(){
  return gulp.src(['dist/*'], {read:false})
  .pipe(clean());
});

gulp.task('injectCompile',function(){
    var target = gulp.src('./dist/index.html');
      // It's not necessary to read the files (will speed up things), we're only after their paths: 
      var sources = gulp.src(['./dist/js/*.js', './dist/styles/*.css'], {read: false});
     
     return target.pipe(inject(sources, {ignorePath: 'dist',addRootSlash: false}))
        .pipe(gulp.dest('./dist'));
});

gulp.task('compress', function () {
    return gulp.src('./app/scripts/**/*.js')
    .pipe(concatJs('concat.js'))
    .pipe(gulp.dest('./.temp/scripts/'))
    .pipe(rename('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

var filesToMove = [
        './app/img/**/*.*',
        './app/styles/**/*.*',
        './app/vendor/**/*.*',
        './app/views/**/*.*',
        './app/index.html'
    ];
gulp.task('moveFiles',function(){
    gulp.src(filesToMove, { base: './app' })
    .pipe(gulp.dest('dist'))
    .on('end', function(){
        gulp.start('injectCompile');
    });

});

gulp.task('compile',gulpsync.sync(['less','css','wiredep','clean','compress','moveFiles']));


